﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace isabel2
{
    class Program
    {
        static void Main(string[] args)
        {
            int moneda;
            string nombre;
            int cantidad;
            Console.ForegroundColor = ConsoleColor.Green;
            List<Producto> productos = new List<Producto>() {
                new Producto("Botella de Agua", 10, 5),
                new Producto("Chocolates", 30, 3),
                new Producto("Limonada ", 15, 10),
                new Producto("Soda", 40, 5),
                new Producto("Zumo", 20, 15),
                new Producto("Jugo de Naranja", 10, 30),
                new Producto("Jugo de Cereza", 15, 14),
                new Producto("Jugo de Uva", 15, 7),
                new Producto("Jugo de Zapote", 25, 18),
                new Producto("Jugo de Fresa", 5, 25),
            };

            Dispensadora dispensadora = new Dispensadora(productos);

            Console.ForegroundColor = ConsoleColor.Blue;
            Console.Write("ingrese la moneda 5,10,25,50,100,200: ");
            Int32.TryParse(Console.ReadLine(), out moneda);

            Console.Write("ingrese el nombre del producto: ");
            nombre = Console.ReadLine();

            Console.Write("ingrese la cantidad del producto: ");
            Int32.TryParse(Console.ReadLine(), out cantidad);

            Producto producto = dispensadora.getProducto(moneda, nombre, cantidad);
            producto.show();
            Console.ReadKey();
        }
    }

    class Dispensadora
    {
        List<Producto> productos = new List<Producto>();
        public Dispensadora(List<Producto> productos)
        {
            this.productos = productos;
        }

        public void addProducto(Producto producto)
        {
            if (this.productos.Count < 10)
            {
                this.productos.Append(producto);
            }
        }
        public Producto getProducto(int moneda, string nombre, int cantidad)
        {
            Producto producto = new Producto("", 0, 0);
            bool isValid = this.verificarMoneda(moneda);
            if (!isValid)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Moneda Invalida");
            }
            else
            {
                foreach (Producto item in productos)
                {
                    if (item.nombre == nombre)
                    {
                        if (item.existencia > cantidad)
                        {
                            item.existencia = item.existencia - cantidad;
                            producto = item;
                        }
                        else if (item.existencia == cantidad)
                        {
                            producto = item;
                            productos.Remove(item);
                        }
                        else
                        {
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine("Lo Sentimos, No hay más productos Actualmente. ");
                        }
                    }
                }
            }
            return producto;
        }

        public bool verificarMoneda(int moneda)
        {
            bool isValid = false;
            switch (moneda)
            {
                case 5:
                    isValid = true;
                    break;
                case 10:
                    isValid = true;
                    break;
                case 25:
                    isValid = true;
                    break;
                case 50:
                    isValid = true;
                    break;
                case 100:
                    isValid = true;
                    break;
                case 200:
                    isValid = true;
                    break;
                default:
                    break;
            }

            return isValid;
        }
    }

    class Producto
    {
        public string nombre;
        public int precio;
        public int existencia;
        public Producto(string nombre, int precio, int existencia)
        {
            this.nombre = nombre;
            this.precio = precio;
            this.existencia = existencia;
        }public void show()
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Nombre: {0} Precio: {1} Existencia: {2}", nombre, precio, existencia);
        }
    }
}