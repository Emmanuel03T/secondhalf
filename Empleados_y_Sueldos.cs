﻿using System;

namespace consoleapp32
{
    class Salarios
    {
        private string[] empleados;
        private double[] sueldos;
        public int y = 1;
        public void Cargar()
        {
            empleados = new string[5];
            sueldos = new double[5];
            for (int f = 0; f < empleados.Length; f++)
            {
                Console.Write("Ingrese el Nombre del Empleado " + y + ": ");
                empleados[f] = Console.ReadLine();
                Console.Write("Ingresar el Sueldo : ");
                string linea;
                linea = Console.ReadLine();
                Console.WriteLine();
                sueldos[f] = int.Parse(linea);
                y++;
            }
        }
        public void MayorSueldo()
        {
            double mayor;
            int m1;
            mayor = sueldos[0];
            m1 = 0;
            for (int f = 1; f < empleados.Length; f++)
            {
                if (sueldos[f] > mayor)
                {
                    mayor = sueldos[f];
                    m1 = f;
                }
            }
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("El Empleado con Mayor Sueldo es : " + empleados[m1]);
            Console.WriteLine("Tiene un Sueldo de: -- " + mayor + " --");
            Console.ReadKey();
        }
        static void Main(string[] args)
        {
            Salarios pv = new Salarios();
            pv.Cargar();
            pv.MayorSueldo();
        }
    }
}